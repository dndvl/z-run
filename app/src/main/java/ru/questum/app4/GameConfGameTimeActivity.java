package ru.questum.app4;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;

public class GameConfGameTimeActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_conf_game_time);

        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        Intent intent = getIntent();

        timePicker.setHour( intent .getIntExtra("hour", 0) );
        timePicker.setMinute( intent .getIntExtra("minute", 0) );
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void save(View view) {
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        int hour = timePicker.getHour();
        int minute = timePicker.getMinute();

        if(hour > 19) {
            hour = 19;
            minute = 59;
        }

        if(hour == 0 && minute == 0) minute = 1;

        Intent intent = this.getIntent();
        intent.putExtra("hour", hour);
        intent.putExtra("minute", minute);
        this.setResult(RESULT_OK, intent);

        this.finish();
    }


    public void cancel(View view) {
        this.finish();
    }
}
