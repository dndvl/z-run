package ru.questum.app4;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;


public class BoxConf extends AppState {

    private int shockZombie_currentMinute;
    private int shockZombie_currentSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_conf);

        shockZombie_currentMinute = shockZombie_minute;
        shockZombie_currentSecond = shockZombie_second;

        Switch pointModeSwitch = (Switch) findViewById(R.id.pointModeSwitch);
        // Set demo mode state
        pointModeSwitch.setChecked(pointMode);


        Switch lightModeSwitch = (Switch) findViewById(R.id.lightModeSwitch);
        // Set game mode 2 state
        lightModeSwitch.setChecked(lightMode);


        EditText pointIDRowValue = (EditText) findViewById(R.id.pointIDRowValue);
        pointIDRowValue.setFilters(new InputFilter[]{new InputFilterMinMax("0", "300")});
        pointIDRowValue.setText( "" + pointID );


        EditText pointsForVictoryValue = (EditText) findViewById(R.id.pointsForVictoryValue);
        pointsForVictoryValue.setText( "" + pointsForVictory );


        // Rename freezing time button
        setFreezingTimeButtonText(shockZombie_minute, shockZombie_second);



        SeekBar brightnessSeekbar = (SeekBar) findViewById(R.id.brightnessValue);
        // Set brightness
        brightnessSeekbar.setProgress(brightness);

    }



    public void changeShockZombie(View view) {
        Intent intent = new Intent(BoxConf.this, BoxConfShockZombie.class);
        int[] shockZombieIntArray = buttonTextToInt( (Button) findViewById(R.id.freezingTimeButton) );
        intent.putExtra("minute", shockZombie_currentMinute);
        intent.putExtra("second", shockZombie_currentSecond);
        startActivityForResult(intent, BOX_CONF__SHOCK_ZOMBIE__REQUEST);
    }

    // Set game time button text
    public void setFreezingTimeButtonText(Integer minute, Integer second) {
        Button button = (Button)findViewById(R.id.freezingTimeButton);
        button.setText(minute + "м " + second + "с");
    }




    // Result intent
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Change freezing time
        if(requestCode == BOX_CONF__SHOCK_ZOMBIE__REQUEST) {
            if (resultCode == RESULT_OK) {
                shockZombie_currentMinute = data.getIntExtra("minute", 0);
                shockZombie_currentSecond = data.getIntExtra("second", 0);

                // Rename freezing time button
                setFreezingTimeButtonText( data.getIntExtra("minute", 0), data.getIntExtra("second", 0) );
            }
        }
    }



    // Close intent with save
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void save(View view) {
        Intent intent = this.getIntent();
        this.setResult(RESULT_OK, intent);

        Switch pointModeSwitch = (Switch) findViewById(R.id.pointModeSwitch);
        pointMode = pointModeSwitch.isChecked();

        Switch lightModeSwitch = (Switch) findViewById(R.id.lightModeSwitch);
        lightMode = lightModeSwitch.isChecked();

        EditText pointIDRowValue = (EditText) findViewById(R.id.pointIDRowValue);
        pointID = Integer.parseInt( pointIDRowValue.getText().toString() );

        EditText pointsForVictoryValue = (EditText) findViewById(R.id.pointsForVictoryValue);
        pointsForVictory = Integer.parseInt( pointsForVictoryValue.getText().toString() );

        shockZombie_minute = shockZombie_currentMinute;
        shockZombie_second = shockZombie_currentSecond;

        SeekBar brightnessSeekbar = (SeekBar) findViewById(R.id.brightnessValue);
        brightness = brightnessSeekbar.getProgress();

        this.finish();
    }

    // Close intent without save
    public void cancel(View view) {
        this.finish();
    }
}



class InputFilterMinMax implements InputFilter {
    private int min, max;

    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMax(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) { }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}