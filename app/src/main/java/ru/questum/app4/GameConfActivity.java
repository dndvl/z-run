package ru.questum.app4;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Switch;


public class GameConfActivity extends AppState {

    private int gameTime_currentHour;
    private int gameTime_currentMinute;

    private int incubationPeriod_currentHour;
    private int incubationPeriod_currentMinute;
    private int incubationPeriod_currentSecond;

    private int shock_currentMinute;
    private int shock_currentSecond;

    private int roulette_currentMinute;
    private int roulette_currentSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_conf);



        Switch zombieModeSwitch = (Switch) findViewById(R.id.zombieModeSwitch);

        // Set game mode 2 state
        zombieModeSwitch.setChecked(zombieMode);



        Switch rouletteModeSwitch = (Switch) findViewById(R.id.rouletteModeSwitch);

        // Set roulette mode state
        rouletteModeSwitch.setChecked(rouletteMode);



        CheckBox zombieInfectCheckbox = (CheckBox) findViewById(R.id.zombieInfectValue);

        // Set zombie infect state
        zombieInfectCheckbox.setChecked(zombieInfect);




        gameTime_currentHour = gameTime_hour;
        gameTime_currentMinute = gameTime_minute;

        // Rename game time button
        setGameTimeButtonText();



        incubationPeriod_currentHour = incubationPeriod_hour;
        incubationPeriod_currentMinute = incubationPeriod_minute;
        incubationPeriod_currentSecond = incubationPeriod_second;

        // Rename incubation period button
        setIncubationPeriodButtonText();



        shock_currentMinute = shock_minute;
        shock_currentSecond = shock_second;

        // Rename stop zombie button
        setStopZombieButtonText();



        roulette_currentMinute = roulette_minute;
        roulette_currentSecond = roulette_second;

        // Rename roulette button
        setRouletteButtonText();
    }






    public void changeGameTime(View view) {
        Intent intent = new Intent(GameConfActivity.this, GameConfGameTimeActivity.class);
        intent.putExtra("hour", gameTime_currentHour);
        intent.putExtra("minute", gameTime_currentMinute);
        startActivityForResult(intent, GAME_CONF__GAME_TIME__REQUEST);
    }

    // Set game time button text
    public void setGameTimeButtonText() {
        Button button = (Button)findViewById(R.id.gameTimeButton);
        button.setText(gameTime_currentHour + "ч " + gameTime_currentMinute + "м");
    }



    public void changeIncubationPeriod(View view) {
        Intent intent = new Intent(GameConfActivity.this, GameConfIncubationPeriodActivity.class);
        intent.putExtra("hour", incubationPeriod_currentHour);
        intent.putExtra("minute", incubationPeriod_currentMinute);
        intent.putExtra("second", incubationPeriod_currentSecond);
        startActivityForResult(intent, GAME_CONF__INCUBATION_PERIOD__REQUEST);
    }

    // Set game time button text
    public void setIncubationPeriodButtonText() {
        Button button = (Button)findViewById(R.id.incubationPeriodButton);
        button.setText(incubationPeriod_currentHour + "ч " + incubationPeriod_currentMinute + "м " + incubationPeriod_currentSecond + "с");
    }


    public void changeShock(View view) {
        Intent intent = new Intent(GameConfActivity.this, GameConfShockActivity.class);
        intent.putExtra("minute", shock_currentMinute);
        intent.putExtra("second", shock_currentSecond);
        startActivityForResult(intent, GAME_CONF__SHOCK__REQUEST);
    }

    // Set game time button text
    public void setStopZombieButtonText() {
        Button button = (Button)findViewById(R.id.stopZombieButton);
        button.setText(shock_currentMinute + "м " + shock_currentSecond + "с");
    }


    public void changeRoulette(View view) {
        Intent intent = new Intent(GameConfActivity.this, GameConfRouletteActivity.class);
        intent.putExtra("minute", roulette_currentMinute);
        intent.putExtra("second", roulette_currentSecond);
        startActivityForResult(intent, GAME_CONF__ROULETTE__REQUEST);
    }

    // Set roulette button text
    public void setRouletteButtonText() {
        Button button = (Button)findViewById(R.id.rouletteButton);
        button.setText(roulette_currentMinute + "м " + roulette_currentSecond + "с");
    }




    // Result intent
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Change game time
        if(requestCode == GAME_CONF__GAME_TIME__REQUEST) {
            if (resultCode == RESULT_OK) {
                gameTime_currentHour = data.getIntExtra("hour", 0);
                gameTime_currentMinute = data.getIntExtra("minute", 0);

                // Rename game time button
                setGameTimeButtonText();
            }
        }
        // Change incubation period
        if(requestCode == GAME_CONF__INCUBATION_PERIOD__REQUEST) {
            if (resultCode == RESULT_OK) {
                incubationPeriod_currentHour = data.getIntExtra("hour", 0);
                incubationPeriod_currentMinute = data.getIntExtra("minute", 0);
                incubationPeriod_currentSecond = data.getIntExtra("second", 0);

                // Rename incubation period button
                setIncubationPeriodButtonText();
            }
        }
        // Change incubation period
        if(requestCode == GAME_CONF__SHOCK__REQUEST) {
            if (resultCode == RESULT_OK) {
                shock_currentMinute = data.getIntExtra("minute", 0);
                shock_currentSecond = data.getIntExtra("second", 0);

                // Rename stop zombie button
                setStopZombieButtonText();
            }
        }
        // Change incubation period
        if(requestCode == GAME_CONF__ROULETTE__REQUEST) {
            if (resultCode == RESULT_OK) {
                roulette_currentMinute = data.getIntExtra("minute", 0);
                roulette_currentSecond = data.getIntExtra("second", 0);

                // Rename roulette button
                setRouletteButtonText();
            }
        }
    }



    // Close intent with save
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void save(View view) {
        Intent intent = this.getIntent();
        this.setResult(RESULT_OK, intent);

        Switch gameMode2Switch = (Switch) findViewById(R.id.zombieModeSwitch);
        zombieMode = gameMode2Switch.isChecked();

        Switch rouletteModeSwitch = (Switch) findViewById(R.id.rouletteModeSwitch);
        rouletteMode = rouletteModeSwitch.isChecked();

        CheckBox zombieInfectCheckbox = (CheckBox) findViewById(R.id.zombieInfectValue);
        zombieInfect = zombieInfectCheckbox.isChecked();

        gameTime_hour = gameTime_currentHour;
        gameTime_minute = gameTime_currentMinute;

        incubationPeriod_hour = incubationPeriod_currentHour;
        incubationPeriod_minute = incubationPeriod_currentMinute;
        incubationPeriod_second = incubationPeriod_currentSecond;

        shock_minute = shock_currentMinute;
        shock_second = shock_currentSecond;

        roulette_minute = roulette_currentMinute;
        roulette_second = roulette_currentSecond;

        this.finish();
    }

    // Close intent without save
    public void cancel(View view) {
        this.finish();
    }
}
