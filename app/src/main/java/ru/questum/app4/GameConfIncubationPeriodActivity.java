package ru.questum.app4;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

public class GameConfIncubationPeriodActivity extends AppState {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_conf_incubation_period);

        Intent intent = getIntent();

        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hour);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(19);
        hourPicker.setValue( intent.getIntExtra("hour", 0) );

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minute);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setValue( intent.getIntExtra("minute", 0) );

        NumberPicker secondPicker = (NumberPicker) findViewById(R.id.second);
        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(59);
        secondPicker.setValue( intent.getIntExtra("second", 0) );
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void save(View view) {
        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hour);
        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minute);
        NumberPicker secondPicker = (NumberPicker) findViewById(R.id.second);

        Intent intent = this.getIntent();
        intent.putExtra( "hour", hourPicker.getValue() );
        intent.putExtra( "minute", minutePicker.getValue() );
        intent.putExtra( "second", secondPicker.getValue() );
        this.setResult(RESULT_OK, intent);

        this.finish();
    }


    public void cancel(View view) {
        this.finish();
    }
}
