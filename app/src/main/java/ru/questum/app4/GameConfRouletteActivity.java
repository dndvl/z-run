package ru.questum.app4;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.NumberPicker;

public class GameConfRouletteActivity extends AppState {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_conf_roulette);

        Intent intent = getIntent();

        NumberPicker hourPicker = (NumberPicker) findViewById(R.id.hour);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(0);
        hourPicker.setValue(0);

        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minute);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(60);
        minutePicker.setValue( intent.getIntExtra("minute", 0) );

        NumberPicker secondPicker = (NumberPicker) findViewById(R.id.second);
        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(59);
        secondPicker.setValue( intent.getIntExtra("second", 0)  );
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void save(View view) {
        NumberPicker minutePicker = (NumberPicker) findViewById(R.id.minute);
        NumberPicker secondPicker = (NumberPicker) findViewById(R.id.second);

        int minute = minutePicker.getValue();
        int second = secondPicker.getValue();

        if(minute == 60) second = 0;

        Intent intent = this.getIntent();
        intent.putExtra( "minute", minute );
        intent.putExtra( "second", second );
        this.setResult(RESULT_OK, intent);

        this.finish();
    }


    public void cancel(View view) {
        this.finish();
    }
}
