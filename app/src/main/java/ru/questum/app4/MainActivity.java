package ru.questum.app4;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppState {

    private BluetoothSocket clientSocket;
    BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Device does not support Bluetooth
        if (bluetooth == null) {
            new AlertDialog.Builder(this)
                    .setTitle("Нет поддержки Bluetooth")
                    .setMessage("Работа приложения на данном устройстве невозможна")
                    .setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        // Bluetooth disabled
        else if (!bluetooth.isEnabled()) {
            bluetoothEnable();
        }

        // Connect to Device
        else {
            bluetoothDevicesDialog();
        }

    }

    private void bluetoothDevicesDialog() {

        // list of devices
        List<String> devicesList = new ArrayList<String>();
        List<String> deviceMacAddressesList = new ArrayList<String>();

        Set<BluetoothDevice> pairedDevices = bluetooth.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                devicesList.add( device.getName() );
                deviceMacAddressesList.add( device.getAddress() );
            }
        }

        final CharSequence[] devices = devicesList.toArray(new CharSequence[devicesList.size()]);
        final CharSequence[] deviceMacAddresses = deviceMacAddressesList.toArray(new CharSequence[deviceMacAddressesList.size()]);


        // select device dialog
        AlertDialog.Builder deviceSelectDialog = new AlertDialog.Builder( this );
        deviceSelectDialog.setCancelable( false );

        deviceSelectDialog.setTitle( "Выберите устройство" );
        deviceSelectDialog.setItems(devices, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String deviceMac = deviceMacAddresses[item].toString();
                bluetoothConnectDevice(deviceMac);
            }
        });

        AlertDialog dialog = deviceSelectDialog.create();
        dialog.show();

    }

    private void bluetoothConnectDevice(String deviceMac) {
        try{
            BluetoothDevice device = bluetooth.getRemoteDevice( deviceMac );
            Method m = device.getClass().getMethod( "createRfcommSocket", new Class[] {int.class} );
            clientSocket = (BluetoothSocket) m.invoke(device, 1);
            clientSocket.connect();
            readConf();
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            Toast.makeText( getApplicationContext(), "Ошибка подключения", Toast.LENGTH_SHORT ).show();
            bluetoothDevicesDialog();
        }
    }

    private void bluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, ENABLE_BT__REQUEST);
    }


    private void readConf() {
        try {
            clientSocket.getOutputStream().write( "8".getBytes() );

            // read from device
            InputStream inputStream = clientSocket.getInputStream();
            String s = "";
            while (s.length() < 45) {
                try {
                    s += inputStream.read();
                } catch (IOException e) {
                    break;
                }
            }

            // set config
            if(s.length() == 45 && Objects.equals(readInt(s, 1, 2), 99) && Objects.equals(readInt(s, 44, 2), 99)) {
                                                                    // 1, 2 - verify (99)
                zombieMode = Objects.equals(readInt(s, 3), 1);      // 3
                rouletteMode = Objects.equals(readInt(s, 4), 1);    // 4
                zombieInfect = Objects.equals(readInt(s, 5), 1);    // 5

                gameTime_hour = readInt(s, 6, 2);                   // 6, 7   - hours
                gameTime_minute = readInt(s, 8, 2);                 // 8, 9   - minutes
                                                                    // 10, 11 - seconds

                incubationPeriod_hour = readInt(s, 12, 2);         // 12, 13
                incubationPeriod_minute = readInt(s, 14, 2);       // 14, 15
                incubationPeriod_second = readInt(s, 16, 2);       // 16, 17

                                                                   // 18, 19 - hours
                shock_minute = readInt(s, 20, 2);                  // 20, 21   - minutes
                shock_second = readInt(s, 22, 2);                  // 22, 23   - seconds

                                                                   // 24, 25 - hours
                roulette_minute = readInt(s, 26, 2);               // 26, 27 - minutes
                roulette_second = readInt(s, 28, 2);               // 28, 29 - seconds

                pointMode = Objects.equals(readInt(s, 30), 1);     // 30
                lightMode = Objects.equals(readInt(s, 31), 1);     // 31

                pointID = readInt(s, 32, 3);                       // 32, 33, 34

                pointsForVictory = readInt(s, 35, 2);              // 35, 36

                                                                   // 37, 38 - hours
                shockZombie_minute = readInt(s, 39, 2);            // 39, 40 - minutes
                shockZombie_second = readInt(s, 41, 2);            // 41, 42 - seconds

                brightness = readInt(s, 43) - 1;                   // 43

                                                                   // 44, 45 - verify (99)

                Toast.makeText( getApplicationContext(), "Ok", Toast.LENGTH_SHORT ).show();
            } else {
                throw new IOException();
            }



        } catch (IOException e) {
            Log.d("BLUETOOTH", e.getMessage());
            Toast.makeText( getApplicationContext(), "Ошибка чтения", Toast.LENGTH_SHORT ).show();
            bluetoothDevicesDialog();
        }
    }



    private void sendConf() {

        try {
            String s = "";
            s += "99";   // 1,2 - verify
            s += zombieMode ? "1" : "0";   // 3 - Режим человек / зомби
            s += rouletteMode ? "1" : "0";   // 4 - Рулетка вкл / выкл
            s += zombieInfect ? "1" : "0";   // 5 - Режим заражения вкл / выкл
            s += intToStringDozen(gameTime_hour) + intToStringDozen(gameTime_minute) + "00"; // 6,7,8,9,10,11 - Время игры (чч,мм,сс)
            s += intToStringDozen(incubationPeriod_hour) + intToStringDozen(incubationPeriod_minute) + intToStringDozen(incubationPeriod_second);   // 12,13,14,15,16,17 - Инкубация (чч,мм,сс)
            s += "00" + intToStringDozen(shock_minute) + intToStringDozen(shock_second);   // 18,19,20,21,22,23 - Шок (чч,мм,сс)
            s += "00" + intToStringDozen(roulette_minute) + intToStringDozen(roulette_second);   // 24,25,26,27,28,29 - Рулетка (чч,мм,сс)

            s += pointMode ? "1" : "0";   // 30 - Режим медпункт / точка
            s += lightMode ? "1" : "0";   // 31 - Режим свет / звук
            s += intToStringHundred(pointID); // 32,33,34 - Точка ID
            s += intToStringDozen(pointsForVictory);   // 35,36 - Точек для победы
            s += "00" + intToStringDozen(shockZombie_minute) + intToStringDozen(shockZombie_second);   // 37,38,39,40,41,42 - Шок зомби (чч,мм,сс)
            s += ( brightness + 1 );   // 43 - Яркость
            s += "99";   // 44,45 - verify


            clientSocket.getOutputStream().write( s.getBytes() );
            Toast.makeText( getApplicationContext(), "Ok", Toast.LENGTH_SHORT ).show();

        } catch (IOException e) {
            Log.d("BLUETOOTH", e.getMessage());
            Toast.makeText( getApplicationContext(), "Ошибка записи", Toast.LENGTH_SHORT ).show();
            bluetoothDevicesDialog();
        }
    }

    private void sendPlayStatus() {
        try {
            clientSocket.getOutputStream().write( "0".getBytes() );
            Toast.makeText( getApplicationContext(), "Ok", Toast.LENGTH_SHORT ).show();

        } catch (IOException e) {
            Log.d("BLUETOOTH", e.getMessage());
            Toast.makeText( getApplicationContext(), "Ошибка записи", Toast.LENGTH_SHORT ).show();
            bluetoothDevicesDialog();
        }
    }



    // Result intent
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Change game config
        if(requestCode == GAME_CONF__REQUEST) {
            if (resultCode == RESULT_OK) {
                sendConf();
            }
        }

        // Change point config
        if(requestCode == BOX_CONF__REQUEST) {
            if (resultCode == RESULT_OK) {
                sendConf();
            }
        }

        // Bluetooth
        if(requestCode == ENABLE_BT__REQUEST) {
            if (resultCode == RESULT_OK) {
                bluetoothDevicesDialog();
            } else if (resultCode == RESULT_CANCELED) {
                bluetoothEnable();
            }
        }
    }



    // Open game mode configuration intent
    public void viewGoToGameMode(View view) {
        Intent intent = new Intent(MainActivity.this, GameConfActivity.class);
        startActivityForResult(intent, GAME_CONF__REQUEST);
    }

    // Open configuration intent
    public void viewGoToBoxConf(View view) {
        Intent intent = new Intent(MainActivity.this, BoxConf.class);
        startActivityForResult(intent, BOX_CONF__REQUEST);
    }

    public void viewPlayStatus(View view) {
        sendPlayStatus();
    }


}