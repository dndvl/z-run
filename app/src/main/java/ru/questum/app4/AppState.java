package ru.questum.app4;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

class AppState extends AppCompatActivity {
    final static int ENABLE_BT__REQUEST = 0;

    final static int GAME_CONF__REQUEST = 10;
    final static int GAME_CONF__GAME_TIME__REQUEST = 11;
    final static int GAME_CONF__INCUBATION_PERIOD__REQUEST = 12;
    final static int GAME_CONF__SHOCK__REQUEST = 13;
    final static int GAME_CONF__ROULETTE__REQUEST = 14;

    final static int BOX_CONF__REQUEST = 20;
    final static int BOX_CONF__SHOCK_ZOMBIE__REQUEST = 21;


    // Человек / Зомби
    static Boolean  zombieMode = false;

    // Рулетка
    static Boolean  rouletteMode   = false;

    // Заражение
    static Boolean  zombieInfect   = false;

    // Время игры
    static int      gameTime_hour   = 2;
    static int      gameTime_minute = 0;

    // Инкубационный период
    static int      incubationPeriod_hour    = 2;
    static int      incubationPeriod_minute  = 0;
    static int      incubationPeriod_second  = 0;

    // Шок
    static int      shock_minute = 0;
    static int      shock_second = 0;

    // Рулетка
    static int      roulette_minute = 1;
    static int      roulette_second = 0;

    // Медпункт / Точка
    static Boolean  pointMode   = false;

    // Звук / Свет
    static Boolean  lightMode   = false;





    // Точка ID
    static int      pointID = 0;

    // Точек для победы
    static int      pointsForVictory = 3;

    // Шок зомби
    static int      shockZombie_minute = 1;
    static int      shockZombie_second = 0;

    // Яркость
    static int      brightness = 7;





    int[] buttonTextToInt(Button button) {
        String string = (String) button.getText();
        String[] stringArray = string.split(" ");
        int[] intArray = new int[ stringArray.length ];
        for(int i = 0; i < stringArray.length; i++) {
            intArray[i] = Integer.parseInt( stringArray[i].replaceAll( "[\\D]", "" ) );
        }
        return intArray;
    }

    String intToStringDozen(Integer i) {
        return (i < 10) ? "0" + i.toString() : i.toString();
    }

    String intToStringHundred(Integer i) {
        return (i < 100) ? (i < 10) ? "00" + i.toString() : "0" + i : i.toString();
    }


    int readInt(String s, int pos) {
        return readInt(s, pos, 1);
    }

    int readInt(String s, int posStart, int length) {
        posStart--;
        return Integer.parseInt(s.substring(posStart, posStart + length));
    }
}
